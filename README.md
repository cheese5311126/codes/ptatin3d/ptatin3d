# pTatin3D

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/Iv6uo7vhReCcKrrEf10ldA "SQAaaS silver badge achieved")

pTatin3D is a software package designed for studying long time-scale processes relevant to geodynamics. Unique to this
package is that it provides fast, parallel scalable matrix-free definitions for the Stokes operators which are utilized by a
hybrid geometric-algebraic multigrid preconditioner. pTatin3D is built upon and heavily utilizes functionality from
PETSc. pTatin3D has been used to study continental rifting, subduction and the dynamics of lava domes.

## Documentation

For further information, please visit the pTatin3D 
[Manual](https://bitbucket.org/ptatin/ptatin3d/src/master/doc/manual/pt3d-manual.tex)

## Developers

1. Jed Brown [jed.brown@colorado.edu]
2. Laetitia Le Pourhiet [laetitia.le@pourhiet@upmc.fr]
3. Dave A. May [david.may@earth.ox.ac.uk]
4. Patrick Sanan [patrick.sanan@erdw.ethz.ch]

## License & Copyright

  GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.


